from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
import russian_fields


def to_python(value):
    if isinstance(value, russian_fields.INN):
        return value
    if value is None:
        return value
    return russian_fields.INN(value)

class InnField(serializers.CharField):
    default_error_messages = {"invalid": _("Enter a valid Inn number.")}

    def to_internal_value(self, data):
        inn_field = to_python(data)
        if inn_field and not inn_field.is_valid_control:
            raise ValidationError(self.error_messages["invalid"])
        return inn_field