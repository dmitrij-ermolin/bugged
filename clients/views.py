from rest_framework import generics
from .models import *
from .serializers import *
from rest_framework.response import Response


class ClientListView(generics.ListCreateAPIView):
    queryset = Client.objects.all().order_by('id')
    serializer_class = ClientSerializer
    filterset_fields = [
        'id', 'city', 'company_name', 'address', 'inn', 'kpp', 'ogrn',
        'email', 'phone_number', 'site_url',
    ]
    def list(self, request, *args, **kwargs):
        queryset = Client.objects.all().order_by('id')
        serializers_class = ClientSerializer(queryset, many=True)
        return Response(serializers_class.data)

class ClientView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

