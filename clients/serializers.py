from rest_framework import serializers
from .models import City, Client
from .InnNumberField import InnField
from .OgrnNumberField import OgrnField
from russian_fields import KPPField

class ClientSerializer(serializers.ModelSerializer):
    city = serializers.PrimaryKeyRelatedField(queryset=City.objects.all())
    inn = InnField()
    kpp = KPPField()
    ogrn = OgrnField()

    class Meta:
        model = Client
        fields = (
            'id', 'city', 'company_name', 'address', 'inn', 'kpp', 'ogrn',
            'email', 'phone_number', 'site_url',
        )


