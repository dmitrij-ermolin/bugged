from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
import russian_fields


def to_python(value):
    if isinstance(value, russian_fields.OGRNField):
        return value
    if value is None:
        return value
    return russian_fields.OGRN(value)

class OgrnField(serializers.CharField):
    default_error_messages = {"invalid": _("Enter a valid Ogrn number.")}

    def to_internal_value(self, data):
        ogrn_filed = to_python(data)
        if ogrn_filed and not ogrn_filed.is_valid_control:
            raise ValidationError(self.error_messages["invalid"])
        return ogrn_filed