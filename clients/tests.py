from rest_framework import status
from rest_framework.test import APITestCase
from django.urls import reverse
from .models import City


class ClientsTest(APITestCase):
    def setUp(self):
        self.moscow = City.objects.create(name="Москва")
        self.spb = City.objects.create(name="Санкт-Петербург")
        self.client_data = {
            "city": 1,
            "company_name": "a",
            "address": "a",
            "inn": "3664069397",
            "kpp": "121212121",
            "ogrn": "1053600591197",
            "email": "adadadad@mail.ru",
            "phone_number": "+79001211212",
            "site_url": "http://127.0.0.1:8000/api/clients/"
        }
        self.invalid_client_data = {
            "city": 1,
            "company_name": "a",
            "address": "a",
            "inn": "366406939",
            "kpp": "121212121",
            "ogrn": "1053600591197",
            "email": "adadadad@mail.ru",
            "phone_number": "+79001211212",
            "site_url": "http://127.0.0.1:8000/api/clients/"
        }

    def test_post_clients(self):
        response = self.client.post(reverse('clients-list'), self.client_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_invalid_clients(self):
        response = self.client.post(reverse('clients-list'), self.invalid_client_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_clients(self):
        response = self.client.get(reverse('clients-list'), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

