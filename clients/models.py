from django.db import models
from russian_fields import INNBusinessField, KPPField, OGRNField
from phonenumber_field.modelfields import PhoneNumberField

class City(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Client(models.Model):
    city = models.ForeignKey('City', models.PROTECT, related_name='clients')
    company_name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    inn = INNBusinessField()
    kpp = KPPField()
    ogrn = OGRNField()
    phone_number = PhoneNumberField(max_length=16)
    site_url = models.URLField(max_length=64)

